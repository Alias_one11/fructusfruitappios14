import SwiftUI

// MARK: - Preview
struct SourceLinkView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        SourceLinkView()//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        .previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
    }
}

struct SourceLinkView: View {
    // MARK: - ©Global-PROPERTIES
    /*````````````````````````````*/
    let url: URL? = URL(string: "https://wikipedia.com")

    /*````````````````````````````*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        

        //||_PARENT__=>GroupBox||
        //*****************************/
        GroupBox() {
            // MARK: - Child-HStack
            /**---------------------------------*/
            HStack {
                //__________
                Text("Content source")
                Spacer()// Spaced horizontally
                
                Link("Wikipedia", destination: url!)
                Image(systemName: "arrow.up.right.square")
                
            }///|>END OF >> HStack||
            .font(.footnote)
            /**---------------------------------*/
            
        }//||END__PARENT-GroupBox||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

