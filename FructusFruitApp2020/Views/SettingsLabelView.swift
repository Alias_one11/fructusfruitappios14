import SwiftUI

// MARK: - Preview
struct SettingsLabelView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        SettingsLabelView(labelText: "Fructus",
                          labelSFImage: "info.circle").padding()
        //.preferredColorScheme(.dark)
        .previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
    }
}

struct SettingsLabelView: View {
    // MARK: - ©Global-PROPERTIES
    /*````````````````````````````*/
    var labelText, labelSFImage: String
    /*````````````````````````````*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //*****************************/
        HStack {
            Text(labelText.uppercased()).fontWeight(.bold)
            
            Spacer()// Spaced Horizontally
            
            Image(systemName: labelSFImage)
        }//||END__PARENT-VSTACK||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

