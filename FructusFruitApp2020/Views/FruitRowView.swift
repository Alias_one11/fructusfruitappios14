import SwiftUI

// MARK: - Preview
struct FruitRowView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        FruitRowView(fruit: fruitData[0]).padding(20)
        //.preferredColorScheme(.dark)
        .previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
    }
}

struct FruitRowView: View {
    // MARK: - ©Global-PROPERTIES
    /*````````````````````````````*/
    var fruit: Fruit
    /*````````````````````````````*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>HStack||
        //*****************************/
        HStack(spacing: 8.0) {
            
            Image(fruit.image)
                .renderingMode(.original)
                .resizable()
                .scaledToFit()
                .frame(width: 80, height: 80, alignment: .center)
                .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.3),
                        radius: 3, x: 2, y: 2)
                .background(
                    LinearGradient(gradient: Gradient(colors: fruit.gradientColors),
                                   startPoint: .top, endPoint: .bottom)
                )
                .cornerRadius(8)
            
            // MARK: -∂ Title && Headline
            ///_CHILD__=>VStack<--HStack
            /**---------------------------------*/
            VStack(alignment: .leading, spacing: 5) {
                //__________
                Text(fruit.title)
                    .font(.title2)
                    .bold()
                
                Text(fruit.headline)
                    .font(.caption)
                    .foregroundColor(.secondary)
            }
            /**---------------------------------*/
        }//||END__PARENT-HStack||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

