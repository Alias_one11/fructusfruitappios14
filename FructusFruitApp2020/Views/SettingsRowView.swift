import SwiftUI

// MARK: - Preview
struct SettingsRowView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        Group {
            SettingsRowView(msgText1: "Developer",
                            msgText2: "AliasTheGreat").padding()
            //.preferredColorScheme(.dark)
            //.previewLayout(.sizeThatFits)
                .previewLayout(.fixed(width: 375, height: 60))
            
            SettingsRowView(msgText1: "Website", linkLabel: "SwiftUI Masterclass", linkDestination: "swiftuimasterclass.com").padding()
                .preferredColorScheme(.dark)
                //.previewLayout(.sizeThatFits)
                .previewLayout(.fixed(width: 375, height: 60))
        }
    }
}

struct SettingsRowView: View {
    // MARK: - ©Global-PROPERTIES
    /*````````````````````````````*/
    var msgText1: String
    var msgText2: String? = nil
    var linkLabel: String? = nil
    var linkDestination: String? = nil
    /*````````````````````````````*/
    
    /*©-----------------------------------------©*/
    
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //*****************************/
        VStack {
            //__________
            Divider().padding(.vertical, 4)
            //__________

            
            HStack {
                Text(msgText1)
                    .foregroundColor(.gray)
                Spacer()// Spaced horizontally
                
                if msgText2 != nil {
                    Text(msgText2 ?? "trailingTxt == nil")
                } else if linkLabel != nil && linkDestination != nil {
                    //__________
                    Link(linkLabel!, destination: URL(string: "https://\(linkDestination!)")!)
                    Image(systemName: "arrow.up.right.square")
                        .foregroundColor(.pink)
                } else {
                    EmptyView()
                }

            }
        }//||END__PARENT-VSTACK||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

