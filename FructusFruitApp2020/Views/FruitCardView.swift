import SwiftUI

// MARK: - Preview
struct FruitCardView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        FruitCardView(fruit: fruitData[1])//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        .previewLayout(.fixed(width: 320, height: 640))
    }
}

struct FruitCardView: View {
    // MARK: - ©Global-PROPERTIES
    /*````````````````````````````*/
    var fruit: Fruit
    
    ///©State|--|PROPERTIES
    @State var isAnimating: Bool = false
    
    
    /*````````````````````````````*/

    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>ZSTACK||
        //*****************************/
        ZStack {
            //__________
            ///_CHILD__=>VStack<--ZStack
            /**---------------------------------*/
            // FRUIT : IMAGE
            VStack(spacing: 20.0) {
                //__________
                Image(fruit.image)
                    .resizable()
                    .scaledToFit()
                    .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.15),
                            radius: 8, x: 6, y: 8)
                    // MARK: - scaleEffect
                    /// The image will scale from small to big
                    //@`````````````````````````````````````````````
                    .scaleEffect(isAnimating ? 1.0 : 0.6)
                    //@-`````````````````````````````````````````````
                
                // FRUIT : TITLE
                Text(fruit.title)
                    .foregroundColor(.white)
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .shadow(
                        color:
                            Color(
                                red: 0, green: 0,
                                blue: 0, opacity: 0.15
                            ),
                        radius: 2, x: 2, y: 2)
                
                // FRUIT : HEADLINE
                Text(fruit.headline)
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 16)
                    .frame(maxWidth: 480)
                
                // MARK: -∂ BUTTON
                StartButtonView()

            }
            /**---------------------------------*/
        }//||END__PARENT-ZSTACK||
        //__________
        // MARK: - onAppear
        //@`````````````````````````````````````````````
        ///__________
        .onAppear(perform: {
            withAnimation(.easeOut(duration: 0.5)) {
                isAnimating = true
            }
        })
        //@-`````````````````````````````````````````````
        .frame(minWidth: 0, maxWidth: .infinity,
               minHeight: 0, maxHeight: .infinity,
               alignment: .center)
        .background(
            LinearGradient(
                gradient: Gradient(colors: fruit.gradientColors),
                startPoint: .top, endPoint: .bottom)
        )
        .cornerRadius(20)
        .padding(.horizontal, 20)
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

