import SwiftUI

// MARK: - Preview
struct FruitHeaderView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        FruitHeaderView(fruit: fruitData[0])//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        .previewLayout(.fixed(width: 375, height: 440))
    }
}

struct FruitHeaderView: View {
    // MARK: - ©Global-PROPERTIES
    /*````````````````````````````*/
    var fruit: Fruit
    
    /// - ©State|--|PROPERTIES
    @State var isAnimatingImage: Bool = false
    
    /*````````````````````````````*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>ZStack||
        //*****************************/
        ZStack {
            
            // MARK: - Child-LinearGradient
            /**---------------------------------*/
            LinearGradient(
                gradient: Gradient(colors: fruit.gradientColors),
                startPoint: .topLeading, endPoint: .bottomTrailing
            )
            /**---------------------------------*/
            
            // MARK: - Child-Image
            /**---------------------------------*/
            Image(fruit.image)
                .resizable()
                .scaledToFit()
                .shadow(color:
                            Color(red: 0, green: 0,
                                  blue: 0, opacity: 0.15),
                        radius: 8, x: 6, y: 8)
                .padding(.vertical, 20)
                // For animation scaling
                .scaleEffect(isAnimatingImage ? 1.0 : 6.0)
            /**---------------------------------*/
            
            
            
        }//||END__PARENT-ZStack||
        .frame(height: 440)
        //__________
        // MARK: - onAppear<--animation
        //@`````````````````````````````````````````````
        .onAppear(perform: {
            //__________
            withAnimation(.easeOut(duration: 0.5)) {
                isAnimatingImage = true
            }
        })
        
        //@-`````````````````````````````````````````````
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

