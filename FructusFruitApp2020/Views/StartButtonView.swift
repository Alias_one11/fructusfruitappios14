import SwiftUI

// MARK: - Preview
struct StartButtonView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        StartButtonView()//.padding(.all, 100)
        .preferredColorScheme(.dark)
        .previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
    }
}

struct StartButtonView: View {
    // MARK: - ©Global-PROPERTIES
    /*````````````````````````````*/
    @AppStorage("isOnBoarding") var isOnBoarding: Bool?
    /*````````````````````````````*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //*****************************/
        VStack(spacing: 8.0) {
            
            Button(action: {
                /// When the user taps on this button it will forward the user to the ContentView, not the OnBoardingView...
                isOnBoarding?.toggle()
            },
            label: {
                HStack {
                    Text("Start")
                    
                    Image(systemName: "arrow.right.circle")
                        .imageScale(.large)
                }
                .padding(.horizontal, 16)
                .padding(.vertical, 10)
                .background(
                    Capsule().strokeBorder(Color.white, lineWidth: 1.25)
                )
                /*©---------------------------------©*/
            })
            .accentColor(.white)
            /*©---------------------------------©*/
            
        }//||END__PARENT-VSTACK||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

