import SwiftUI

// MARK: - Preview
struct FruitNutrientsView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        FruitNutrientsView(fruit: fruitData[0]).padding()
        .preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        .previewLayout(.fixed(width: 375, height: 480))
    }
}

struct FruitNutrientsView: View {
    // MARK: - ©Global-PROPERTIES
    /*````````````````````````````*/
    var fruit: Fruit
    let nutrients: [String] = [
        "Energy", "Sugar", "Fat",
        "Protein", "Vitamins", "Minerals"
    ]
    /*````````````````````````````*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>GroupBox||
        //*****************************/
        GroupBox() {
            
            DisclosureGroup("Nutritional value per 100g") {
                //__________
                ForEach(0..<nutrients.count, id: \.self) { item in
                    //__________
                    Divider().padding(.vertical, 2)
                    
                    ///_CHILD__=>HStack<--ForEach
                    /**---------------------------------*/
                    HStack {
                        Group {
                            Image(systemName: "info.circle")
                            Text(nutrients[item])
                        }
                        .foregroundColor(fruit.gradientColors[1])
                        .font(Font.system(.body).bold())
                        /*©---------------------------------©*/
                        Spacer(minLength: 25)// Spaced horizontally .leading
                        
                        Text(fruit.nutrition[item])
                            .multilineTextAlignment(.trailing)
                        
                    }//|>END OF >> HStack||
                    
                    /**---------------------------------*/
                    
                }///|>END OF >> ForEach||
                
                
            }///|>END OF >> DisclosureGroup||
            
        }//||END__PARENT-GroupBox||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

