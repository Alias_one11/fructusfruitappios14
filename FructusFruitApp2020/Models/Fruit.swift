//
//  FruitModel.swift
//  FructusFruitApp2020

import SwiftUI

// MARK: -∂ FRUIT DATA-MODEL
struct Fruit: Identifiable {
    //__________
    // MARK: -∂ ©PROPERTIES
    var id: String = UUID().uuidString
    /*©---------------------------------©*/
    var title, headline, image: String
    var gradientColors: [Color]
    var description: String
    var nutrition: [String]
}
