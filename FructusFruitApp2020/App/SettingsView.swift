import SwiftUI

// MARK: - Preview
struct SettingsView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        SettingsView()//.padding(.all, 100)
        .preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
    }
}

struct SettingsView: View {
    // MARK: - ©Global-PROPERTIES
    /*````````````````````````````*/
    /// - ©Environment
    /// - [presentationMode]: A binding to the current presentation
    /// - mode of the view associated with this environment.
    @Environment(\.presentationMode) var presentationMode
    
    /// - ©AppStorage
    @AppStorage("isOnBoarding") var isOnBoarding: Bool = false
    /*````````````````````````````*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>NavigationView||
        //*****************************/
        NavigationView {
            ///_CHILD__=>ScrollView<--NavigationView
            /**---------------------------------*/
            ScrollView(.vertical, showsIndicators: false) {
                //__________
                VStack(spacing: 20) {
                    // MARK: -∂ SECTION #1
                    /**---------------------------------*/
                    GroupBox(
                        label:
                            SettingsLabelView(labelText: "Fructus",
                                              labelSFImage: "info.circle")
                    ) {
                        //__________
                        Divider().padding(.vertical, 4)
                        //__________
                        
                        HStack(alignment: .center, spacing: 10) {
                            // MARK: -∂ Logo
                            Image("logo")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 80, height: 80)
                                .cornerRadius(9)
                            
                            Text("Most fruits are naturally low in fat, sodium, and calories. None have cholesterol. Fruits are sources, of many potential nutrients, including potassium, dietary fiber, vitamins, and much more.")
                                .font(.footnote)
                        }
                    }
                    /**---------------------------------*/
                    
                    
                    // MARK: -∂ SECTION #2
                    /**---------------------------------*/
                    GroupBox(
                        label:
                            SettingsLabelView(labelText: "Customazation", labelSFImage: "paintbrush")
                    ) {
                        //__________
                        Divider().padding(.vertical, 8)
                        //__________
                        
                        /// - ©Footnote
                        Text("If you wish, you can restart the application by toggling the switch in this box. That way it restarts the onboarding process and you will see the welcome screen again.")
                            .padding(.vertical, 8)
                            .frame(minHeight: 60)
                            .layoutPriority(1)
                            .font(.footnote)
                            .multilineTextAlignment(.leading)
                        
                        // MARK: -∂ Toggle to restart app
                        Toggle(isOn: $isOnBoarding, label: {
                            if isOnBoarding {
                                Text("Restarted".uppercased())
                                    .bold()
                                    .foregroundColor(.green)
                            } else {
                                Text("Restart".uppercased())
                                    .fontWeight(.bold)
                                    .foregroundColor(.secondary)
                            }
                        })
                        .padding()
                        .background(
                            Color(UIColor.tertiarySystemBackground)
                                .clipShape(RoundedRectangle(cornerRadius: 8,
                                                            style: .continuous)
                                )
                        )
                        
                    }
                   
                    /**---------------------------------*/
                    
                    // MARK: -∂ SECTION #3
                    /**---------------------------------*/
                    GroupBox(
                        label:
                            SettingsLabelView(labelText: "Application",
                                              labelSFImage: "apps.iphone")
                    ) {                        
                        // MARK: -∂ Developer
                        SettingsRowView(msgText1: "Developer", msgText2: "AliasTheGreat")
                        // MARK: -∂ Designer
                        SettingsRowView(msgText1: "Designer", msgText2: "Jesse James")
                        // MARK: -∂ Compatibility
                        SettingsRowView(msgText1: "Compatibility", msgText2: "IOS 14")
                        // MARK: -∂ Website-->With link
                        SettingsRowView(msgText1: "Website",
                                        linkLabel: "SwiftUI Masterclass",
                                        linkDestination: "swiftuimasterclass.com")
                        // MARK: -∂ Twitter-->With link
                        SettingsRowView(msgText1: "Twitter",
                                        linkLabel: "@Alias_one11",
                                        linkDestination: "twitter.com/Alias_One11")
                        // MARK: -∂ SwiftUI
                        SettingsRowView(msgText1: "SwiftUI", msgText2: "2.0")
                        // MARK: -∂ Version
                        SettingsRowView(msgText1: "Version", msgText2: "1.1.0")
                        
                    }
                    /**---------------------------------*/
                    
                }///|>END OF >> VStack||
                .navigationBarTitle(Text("Settings"), displayMode: .large)
                //__________
                // MARK: - navigationBarItems
                //@`````````````````````````````````````````````
                .navigationBarItems(
                    trailing:
                        // MARK: -∂ Button
                        Button(action: {
                            presentationMode.wrappedValue.dismiss()
                        }, label: {
                            Image(systemName: "xmark")
                        })
                )
                //@-`````````````````````````````````````````````
                .padding()
                /*©---------------------------------©*/
                
            }///|>END OF >> ScrollView||
            /**---------------------------------*/
            
            
        }//||END__PARENT-NavigationView||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

