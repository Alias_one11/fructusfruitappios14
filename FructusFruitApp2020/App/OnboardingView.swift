import SwiftUI

// MARK: - Preview
struct OnboardingView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        OnboardingView(fruits: fruitData)//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        .previewLayout(.fixed(width: 320, height: 640))
    }
}

struct OnboardingView: View {
    // MARK: - ©Global-PROPERTIES
    /*````````````````````````````*/
    var fruits: [Fruit] = fruitData
    /*````````````````````````````*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>TabView||
        //*****************************/
        /// TabView: A view that switches between multiple child views using interactive user interface elements.
        TabView {
            ///_CHILD__=>ForEach<--TabView
            /**---------------------------------*/
            ForEach(fruits[0...5]) { item in
                FruitCardView(fruit: item)
                
            }///|>END OF >> ForEach||
            /**---------------------------------*/
            
        }//||END__PARENT-TabView||
        /// .tabViewStyle(): Sets the style for the tab view within the the current environment.
        .tabViewStyle(PageTabViewStyle())
        .padding(.vertical, 20)
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

