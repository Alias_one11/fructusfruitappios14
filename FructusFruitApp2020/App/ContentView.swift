import SwiftUI

// MARK: - Preview
struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        ContentView(fruits: fruitData)//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
    }
}

struct ContentView: View {
    // MARK: - ©Global-PROPERTIES
    /*````````````````````````````*/
    var fruits: [Fruit] = fruitData
    
    /// - ©State|--|PROPERTIES
    @State var isShowingSettings: Bool = false
    /*````````````````````````````*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>NavigationView||
        //*****************************/
        NavigationView {
            //__________
            List {
                //__________
                ForEach(fruits.shuffled()) { item in
                    /// Will navigate to the fruit detail view
                    /// when the row is pressed...
                    NavigationLink(
                        destination: FruitDetailView(fruit: item),
                        label: {
                            // MARK: -∂ Navigating to-->FruitRowView()
                            FruitRowView(fruit: item)
                                .padding(.vertical, 8)
                        })
                }
            }///|>END OF >> List||
            .navigationTitle("Fruits")
            //__________
            // MARK: - navigationBarItems
            //@`````````````````````````````````````````````
            .navigationBarItems(
                trailing:
                    // MARK: -∂ Button
                    Button(action: {
                        isShowingSettings = true
                    }, label: {
                        // MARK: -∂ Slider SFSymbol
                        Image(systemName: "slider.horizontal.3")
                    })
                    // Navigates the sheet when toggled to the SettingsView()
                    .sheet(isPresented: $isShowingSettings, content: { SettingsView() })
            
            )
            //@-`````````````````````````````````````````````
            
            /*©---------------------------------©*/
            
        }//||END__PARENT-NavigationView||
        .navigationViewStyle(StackNavigationViewStyle())
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

