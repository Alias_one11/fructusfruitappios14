import SwiftUI

// MARK: - Preview
struct FruitDetailView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        FruitDetailView(fruit: fruitData[0])//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
    }
}

struct FruitDetailView: View {
    // MARK: - ©Global-PROPERTIES
    /*````````````````````````````*/
    var fruit: Fruit
    
    /*````````````````````````````*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>NavigationView||
        //*****************************/
        NavigationView {
            // MARK: - Child-ScrollView
            /**---------------------------------*/
            ScrollView(.vertical, showsIndicators: false) {
                //__________
                ///_CHILD__=>VStack<--ScrollView()
                /**---------------------------------*/
                VStack(alignment: .center, spacing: 20) {
                    //__________
                    // MARK: -∂ HEADER
                    FruitHeaderView(fruit: fruit)
                    
                    VStack(alignment: .leading, spacing: 20) {
                        //__________
                        // MARK: -∂ TITLE
                        Text(fruit.title)
                            .font(.largeTitle)
                            .fontWeight(.heavy)
                            .foregroundColor(fruit.gradientColors[1])
                        
                        // MARK: -∂ HEADLINE
                        Text(fruit.headline)
                            .font(.headline)
                            .multilineTextAlignment(.leading)
                        
                        // MARK: -∂ NUTRIENTS
                        FruitNutrientsView(fruit: fruit)
                        
                        // MARK: -∂ SUBHEADLINE
                        Text("Learn more about \(fruit.title)".uppercased())
                            .fontWeight(.bold)
                            .foregroundColor(fruit.gradientColors[1])
                        
                        // MARK: -∂ DESCRIPTION
                        Text(fruit.description)
                            .multilineTextAlignment(.leading)
                        
                        // MARK: -∂ LINK
                        SourceLinkView()
                            .padding(.top, 10)
                            .padding(.bottom, 40)
                    }
                    .padding(.horizontal, 20)
                    .frame(maxWidth: 640, alignment: .center)
                    /*©---------------------------------©*/
                    
                }///|>END OF >> VStack||
                .navigationBarTitle(fruit.title, displayMode: .inline)
                .navigationBarHidden(true)
                /**---------------------------------*/
                
            }///|>END OF >> ScrollView||
            .edgesIgnoringSafeArea(.top)
            /**---------------------------------*/
            
        }//||END__PARENT-NavigationView||
        .navigationViewStyle(StackNavigationViewStyle())
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

