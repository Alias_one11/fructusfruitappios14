//
//  FruitModel.swift
//  FructusFruitApp2020

import SwiftUI

@main
struct FructusFruitApp2020App: App {
    // MARK: -∂ Property
    @AppStorage("isOnBoarding") var isOnBoarding: Bool = true
    
    var body: some Scene {
        //__________
        WindowGroup {
            //__________
            if isOnBoarding {
                OnboardingView()
            } else { ContentView() }
        }
    }
}
